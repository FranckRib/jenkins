var express = require('express');
var router = express.Router();
var generateMdp = require('./../helper/generateMdp');
/* GET home page. */
router.get('/:nbCar/:flag', function(req, res, next) {

    var mdp = generateMdp(req.params.nbCar, req.params.flag);
    res.render('index', { title: mdp });
});

module.exports = router;