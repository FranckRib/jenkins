module.exports = function generateMdp(nbCar, flag) {
    var mdp = "";
    var l = "abcdefghijklmnopqrstuvwxyz";
    var u = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var n = "0123456789";
    var s = "!#$%&()*+,-./:;<=>?@[\]^_{|}"

    if (flag == 1) {
        while (mdp.length < nbCar) {
            mdp += l[Math.floor(Math.random() * l.length)];
        }
    } else if (flag == 2) {
        while (mdp.length < nbCar) {
            mdp += l[Math.floor(Math.random() * l.length)];
            mdp += u[Math.floor(Math.random() * u.length)];
        }
    } else if (flag == 3) {
        while (mdp.length < nbCar) {
            mdp += l[Math.floor(Math.random() * l.length)];
            mdp += u[Math.floor(Math.random() * u.length)];
            mdp += n[Math.floor(Math.random() * n.length)];
        }
    } else if (flag == 4) {
        while (mdp.length < nbCar) {
            mdp += l[Math.floor(Math.random() * l.length)];
            mdp += u[Math.floor(Math.random() * u.length)];
            mdp += n[Math.floor(Math.random() * n.length)];
            mdp += s[Math.floor(Math.random() * s.length)];
        }
    }

    return mdp;
}