job('Job1') {
    scm {
       git('https://gitlab.com/FranckRib/jenkins'){ node ->
            node / gitConfigName('FranckRib')
            node / gitConfigEmail('kranfc@hotmail.com')
        }
    }
    wrappers {
        nodejs('Node9')
    }
    triggers {
        cron('H/3 * * * *')
    }
    steps {
        shell 'npm install'
        shell 'npm test'
    }
    publishers {
        slackNotifier
        {
            notifyAborted(true)
            notifyFailure(true)
            notifyNotBuilt(true)
            notifyUnstable(true)
            notifyBackToNormal(true)
            notifySuccess(true)
            notifyRepeatedFailure(true)
            startNotification(true)
            includeTestSummary(true)
        }
    }
}