var expect = require("chai").expect;
var assert = require("chai").assert;
var chai = require("chai");

chai.Assertion.addProperty('uppercase', function() {
    var obj = this._obj;
    new chai.Assertion(obj).to.be.a('string');

    this.assert(
        obj === obj.toUpperCase() // adapt as needed
        , 'expected #{this} to be all uppercase' // error message when fail for normal
        , 'expected #{this} to not be all uppercase' // error message when fail for negated
    );
});

var request = require("request");
var generateMdp = require('./../helper/generateMdp');

describe("Example Mocha test", function() {
    describe("test 1", function() {
        it("should be selected length", function() {
            expect(generateMdp(5, 1)).have.lengthOf(5);
        })
    })
    describe("test 2", function() {
        it("should be lowercase", function() {
            // expect(generateMdp(5, 1)).to.not.be.uppercase;
            assert.notMatch(generateMdp(5, 1), /^[A-Z0-9!#$%&()*+,-./:;<=>?@[\]^_{|}]/);
        })
    })
    // describe("test 3", function() {
    //     it("should be mix of lower and uppercase", function() {
    //         assert.match(generateMdp(5, 2), /^[A-Za-z]/);
    //         assert.notMatch(generateMdp(5, 2), /^[A-Z]/);
    //         assert.notMatch(generateMdp(5, 2), /^[a-z]/);
    //         assert.notMatch(generateMdp(5, 1), /^[0-9!#$%&()*+,-./:;<=>?@[\]^_{|}]/);
    //     })
    // })
})